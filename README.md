# Software-AG an der KKS 2022/23
Hier findet ihr Lehrmaterialien und Beispiele für die Software-AG an der KKS.

## Einführung in die Programmierung mit Kotlin
* [Zusammenfassung Woche 1](src/main/kotlin/week1)
* [Zusammenfassung Woche 2](src/main/kotlin/week2)
* [Zusammenfassung Woche 3](src/main/kotlin/week3)
* [Zusammenfassung Woche 4](src/main/kotlin/week4)
* [Zusammenfassung Woche 5](src/main/kotlin/week5)

## Download-Link IntelliJ IDEA
Hier könnt ihr euch die Entwicklungsumgebung
IntelliJ IDEA (Community Edition) herunterladen:
https://www.jetbrains.com/de-de/idea/download/

Alternativ gibt es eine sogenannte portable Version.
Die könnt ihr auch auf den Schulrechnern installieren.
Wählt dafür euer eigenes Netzlaufwerk (H:)
als Installationsort,
dann wird es auf allen Rechnern verfügbar sein,
an denen ihr euch anmeldet.
Die portable Version findet ihr hier
(grüner Download-Button am Ende der Seite):
https://portapps.io/app/intellij-idea-community-portable/

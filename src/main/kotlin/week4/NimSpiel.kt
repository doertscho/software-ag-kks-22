package week4

// Notizen:
// Gesamtanzahl Streichhölzer festlegen
// Zahl entgegennehmen (Mensch macht seinen Zug)
// Computer muss Zahl bestimmen (Computer macht seinen Zug)
// Neue Gesamtanzahl berechnen
// Wurde das letzte Streichholz genommen?
// Wer hat gewonnen?

fun menschlicherZug(): Int {
    println("Du bist an der Reihe, bite gib ein, wie viele Streichhölzer du nehmen möchtest (1, 2 oder 3):")
    val eingabeText = readln()
    val eingabeZahl = eingabeText.toInt()
    return eingabeZahl
}

fun computerZug(): Int {
    return 1
}

val mensch = 1
val computer = 2

// Diese Funktion beschreibt den Ablauf einer einzelnen Spielrunde
fun runde(anzahlHoelzer: Int, aktuellerSpieler: Int) {

    // Zeige aktuellen Spielstand an
    println("Es liegen noch $anzahlHoelzer Streichhölzer auf dem Tisch.")

    // Wie viele Hölzer werden genommen?
    // Wie wir das herausfinden, hängt davon ab, wer gerade an der Reihe ist!
    val gezogeneHoelzer: Int
    if (aktuellerSpieler == mensch) {
        // Wenn der Mensch an der Reihe ist, dann ...
        gezogeneHoelzer = menschlicherZug()
    } else {
        // Wenn der Computer an der Reihe ist, dann ...
        gezogeneHoelzer = computerZug()
        println("Der Computer nimmt $gezogeneHoelzer Hölzchen.")
    }

    // Wie viele Hölzer sind jetzt noch übrig?
    val neueAnzahlHoelzer = anzahlHoelzer - gezogeneHoelzer

    // Ist das Spiel vorbei?
    if (neueAnzahlHoelzer > 0) {

        // Wer ist der nächste Spieler?
        val naechsterSpieler: Int
        if (aktuellerSpieler == mensch) {
            naechsterSpieler = computer
        } else {
            naechsterSpieler = mensch
        }

        // Läute die nächste Runde ein!
        runde(neueAnzahlHoelzer, naechsterSpieler)

    } else {
        // Informationen über das Spielende ausgeben
        println("Das Spiel ist vorbei!")
        if (aktuellerSpieler == mensch) {
            println("Der Mensch gewinnt!")
        } else {
            println("Der Computer gewinnt!")
        }

        // Eine nächste Runde gibt es nicht.
        // Die Programmausführung endet an dieser Stelle.
    }
}

fun main() {

    println("Herzlich willkommen beim Nim-Spiel!")

    val startAnzahl = 21
    val startSpieler = mensch

    // Beginne die erste Runde
    runde(startAnzahl, startSpieler)
}

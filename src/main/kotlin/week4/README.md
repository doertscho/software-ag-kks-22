# Einführung in die Programmierung mit Kotlin – Woche 4

## Entwicklung eines kleinen Spiels, inklusive Computerspieler!

In dieser Woche wollen wir ein kleines Spiel entwickeln
und dafür die Konzepte nutzen,
die wir bisher kennengelernt haben:
Funktionen, Verzweigungen, Ein- und Ausgabe von Text,
und simple mathematische Ausdrücke.
Für allzu komplexe Spiele reicht das noch nicht aus,
aber wenn ihr es geschickt anstellt,
könnt ihr auch mit diesen Mitteln schon einiges umsetzen.

Das Spiel geht wie folgt:
Auf dem Tisch liegen 21 Streichhölzer
(oder Münzen, oder Steine, oder was auch immer – das ist eigentlich egal).
Zwei Personen spielen gegeneinander.
Die erste Person beginnt das Spiel und darf entscheiden,
ob sie ein, zwei oder drei Hölzer wegnimmt.
Dann ist die andere Person an der Reihe,
auch sie darf entweder eines, zwei oder drei der Hölzer wegnehmen.
So geht es immer hin und her,
bis keine Hölzer mehr auf dem Tisch liegen.
Wer das letzte Hölzchen nimmt, hat gewonnen.

Es ist zugegebenermaßen vielleicht nicht das spannendste Spiel der Welt,
aber es eignet sich sehr gut als Programmieraufgabe 😛
In unserem Fall wird eine der zwei Personen der Computer sein,
und wir spielen das Spiel,
indem wir in jeder Runde entweder 1, 2 oder 3 eintippen.

## Kommentare

Bevor wir mit dem Programmieren beginnen,
ist es oft sinnvoll, zunächst ein paar Notizen aufzuschreiben:
Was müssen wir alles bedenken?
Was muss im Ablauf unseres Programms alles passieren?
Solche Notizen können wir auch direkt in unseren Programm-Dateien machen.
Dafür gibt es die sogenannten Kommentare.
Jede Zeile, die mit zwei Schrägstrichen beginnt,
wird von Kotlin ignoriert.
Hier können wir also beliebigen Text hinschreiben,
der kein korrekter Kotlin-Code sein muss.

```
// Das hier ist ein Kommentar!
// Hier kann ich mir beliebige Notizen machen.
// Auch diese Zeile wird ignoriert.

// Hier beginnt gleich meine Hauptfunktion:
fun main() {

    // Als erstes möchte ich die Nutzer*innen meines Programms begrüßen:
    println("Hallo Welt!")
}
```

In unserem Programm [NimSpiel.kt](NimSpiel.kt)
findet ihr diverse Beispiele für Kommentare:
Allgemeine Notizen am Anfang,
Erklärungen zu bestimmten Abschnitten des Codes mittendrin.

## Abstraktion des Spielablaufs

Um effiziente Programme zu entwickeln,
müssen wir uns immer die Frage stellen, 
an welchen Stellen in unserem Programm sich Abläufe und Muster wiederholen.
Wir haben in unserem Energie-Rechner schon ein kleines Beispiel dafür gesehen:
Für mehrere Aufgaben war es erforderlich,
eine Zahl einzulesen.
Diese Teilaufgabe des Programms haben wir darum in eine eigene Funktion ausgelagert,
die wir dann mehrfach wiederverwenden konnten.

In Spielen ist ein guter Anfangspunkt dafür oft die Betrachtung
einer einzelnen Spielrunde.
Denn der Ablauf einer Runde ist immer gleich:
Es gibt eine Gesamtzahl von Hölzern,
die Person entscheidet sich, wie viele sie nehmen möchte,
die Hölzer werden weggenommen,
und hat sie das Spiel entweder gewonnen, oder die nächste Runde beginnt.
Um unser Spiel umzusetzen, erscheint es also sinnvoll,
eine Funktion zu entwickeln,
die den Ablauf einer Runde darstellt.

Anders als in den bisherigen Beispielen
ist in diesem Fall nicht direkt ersichtlich,
was die Eingabe- und Ausgabewerte einer solchen Funktion sind.
Das ist aber in Ordnung.
In einem solchen Fall können wir die Anweisungen so weit aufschreiben,
bis wir zu einem Punkt gelangen,
an dem wir eine Information brauchen,
die uns nicht zur Verfügung steht.
Wenn wir dann feststellen,
dass wir diese auch nicht aus bekannten Daten berechnen können,
müssen wir diese Information vermutlich "von außen" anfordern,
sprich, sie muss Teil der Eingabedaten der Funktion sein.

## Zustand ausgeben

Zum Beginn einer neuen Runde wollen wir zunächst darüber informieren,
wie der aktuelle Zustand des Spiels aussieht –
also in diesem Fall, wie viele Hölzer noch auf dem Tisch liegen.
Hier stoßen wir direkt auf die erste unbekannte Infomation.
Unsere Funktion `runde` soll den Ablauf einer einzelnen Runde abbilden,
in Isolation, also ohne Wissen über die Vergangenheit.
Das heißt, der aktuelle Spielzustand muss von außen in unsere Funktion gebracht werden.
Wir müssen also die Liste der Eingabedaten erweitern.

```
fun runde(anzahlHoelzer: Int) {
    
    println("Es liegen noch $anzahlHoelzer auf dem Tisch.")
}
```

Hier greift wieder das Prinzip der Abstraktion:
Wir wissen, dass wir diese Information an dieser Stelle brauchen.
Wie genau sie dort zur Verfügung gestellt werden kann,
darüber können wir uns an anderer Stelle Gedanken machen.

Ihr seht in diesem Beispiel ein neues Konstrukt in der Zeile
für die Textausgabe:
Der benannte Wert `anzahlHoelzer` ist direkt in den Text integriert.
Das können wir mit Verwendung des Dollar-Zeichens (`$`) machen.
Wenn Kotlin das in einem Text-Wert sieht,
weiß es, dass an dieser Stelle der Wert eingesetzt werden soll,
der unter dem entsprechenden Namen gespeichert ist.

## Einen Zug machen

Der nächste Schritt in einer jeden Runde besteht darin,
dass der nächste Zug ermittelt wird.
Es muss also abgefragt werden,
wie viele Hölzer der*die aktuelle Spieler*in wegnehmen möchte.
Diese Information wollen wir uns natürlich speichern,
wir können also einen neuen Wert einführen,
den wir zum Beispiel `gezogeneHoelzer` nennen können.

Aber: Welchen Wert weisen wir hier zu?
Das hängt davon ab, ob der Computer oder der Mensch an der Reihe ist.
In einer solchen Situation,
in der wir wissen, dass wir einen benannten Wert brauchen,
aber nicht direkt hinschreiben können,
wie der zugewiesene Wert dafür bestimmt wird,
können wir die Zuweisung zunächst weglassen.
Wir müssen Kotlin dann aber zumindest mitteilen,
von welchem Datentyp unser benannter Wert sein soll.
Das sieht dann so aus:

```
val gezogeneHoelzer: Int
// In einer der folgenden Zeilen kann ich nun einen Wert zuweisen!
gezogeneHoelzer = ...
```

Das ist insbesondere dann nützlich,
wenn wir eine Verzweigung nutzen wollen,
und dem benannten Wert `gezogeneHoelzer` in Abhängigkeit von bestimmten Bedingungen
unterschiedliche Werte zuweisen wollen.
Das ist hier der Fall.
Wenn der Mensch an der Reihe ist,
wollen wir dem Menschen die Chance geben, eine Zahl einzutippen,
ansonsten wollen wir den Computer-Spieler befragen.
Wie genau wir das machen, das können wir uns wieder später überlegen,
und zunächst einfach auf noch nicht existierende Funktionen verweisen.
Aber davor stellt sich eine andere Frage:
Woher wissen wir, wer an der Reihe ist?

```
val gezogeneHoelzer: Int
if ( ... Mensch ist an der Reihe ... ) {
    gezogeneHoelzer = menschlicherZug()
} else {
    gezogeneHoelzer = computerZug()
}
```

Ähnlich wie mit der Gesamtanzahl von Hölzern zu Beginn der Runde
handelt es sich hierbei um eine Information,
die wir nicht innerhalb unserer Funktion berechnen können.
Auch diese Information muss unserer Funktion von außen zur Verfügung gestellt werden.
Wir müssen die Liste der Eingabedaten um einen weiteren Wert ergänzen.

Doch von welchem Typ ist dieser Wert?
Bei der Anzahl der Hölzer ist recht klar,
dass es sich um eine ganze Zahl handeln muss.
Für die Information, wer an der Reihe ist,
passt keiner der bisher bekannten Datentypen so richtig.

Auch hier gibt es mehrere Möglichkeiten,
diese Information darzustellen.
Der Vorschlag aus unserer Runde war es,
hierfür auch Zahlen zu nutzen.
Der Mensch wäre dann beispielsweise Nummer 1, der Computer Nummer 2.
Die Funktion könnten wir dann so ergänzen:

```
fun runde(anzahlHoelzer: Int, aktuellerSpieler: Int) {

    // Zeige aktuellen Spielstand an
    println("Es liegen noch $anzahlHoelzer Streichhölzer auf dem Tisch.")

    // Wie viele Hölzer werden genommen?
    // Wie wir das herausfinden, hängt davon ab, wer gerade an der Reihe ist!
    val gezogeneHoelzer: Int
    if (aktuellerSpieler == 1) {
        // Wenn der Mensch an der Reihe ist, dann ...
        gezogeneHoelzer = menschlicherZug()
    } else {
        // Wenn der Computer an der Reihe ist, dann ...
        gezogeneHoelzer = computerZug()
        println("Der Computer nimmt $gezogeneHoelzer Hölzchen.")
    }
}
```

Das funktioniert!
Einen Nachteil hat diese Lösung allerdings:
Wenn sich eine andere Person nun später euren Code anschaut,
dann weiß sie nichts davon,
dass wir diese Festlegung (Mensch = 1, Computer = 2) so gemacht haben.
Sie würde lesen "wenn aktueller Spieler gleich 1 ...",
und würde sich vermutlich fragen, was das denn bedeuten soll.
Für den Computer, der unser Programm ausführt, ist das egal.
Der arbeitet einfach die Befehle ab und interessiert sich nicht dafür,
was die Intention dahinter war.
Für Menschen ist es aber verwirrend.


## Konstanten statt Magic Numbers

Unter Entwickler*innen wird so eine Situation auch als "Magic Number" bezeichnet.
Zahlen tauchen im Code auf, und der Code scheint zu funktionieren,
aber warum das so ist, weiß man nicht – es muss sich wohl um Magie handeln.
Darum gibt es den Grundsatz,
solche Zahlen nicht direkt ohne weiteres in den Programmcode zu schreiben,
sondern ihnen Namen zu geben, die ihren Zweck beschreiben.

Das können wir mit dem bekannten Prinzip der benannten Werte machen.
In solchen Fällen, in denen eine Zuweisung immer einen festen Wert hat,
können wir die Definition der Werte sogar außerhalb einer Funktion vornehmen.
Die benannten Werte sind dann im ganzen Programm verfügbar.
Solche Werte werden dann auch als Konstanten bezeichnet.
Wir können unser Programm also wie folgt ändern:

```
// Codierung der teilnehmenden Spieler
val mensch = 1
val computer = 2

// Diese Funktion beschreibt den Ablauf einer einzelnen Spielrunde
fun runde(anzahlHoelzer: Int, aktuellerSpieler: Int) {

    // Zeige aktuellen Spielstand an
    println("Es liegen noch $anzahlHoelzer Streichhölzer auf dem Tisch.")

    // Wie viele Hölzer werden genommen?
    // Wie wir das herausfinden, hängt davon ab, wer gerade an der Reihe ist!
    val gezogeneHoelzer: Int
    if (aktuellerSpieler == mensch) {
        // Wenn der Mensch an der Reihe ist, dann ...
        gezogeneHoelzer = menschlicherZug()
    } else {
        // Wenn der Computer an der Reihe ist, dann ...
        gezogeneHoelzer = computerZug()
        println("Der Computer nimmt $gezogeneHoelzer Hölzchen.")
    }
}
```

Jetzt liest sich die Verzweigung fast wie ein deutscher Satz:
"wenn aktueller Spieler gleich Mensch,
dann menschlicher Zug, ansonsten Computer-Zug".
Aus Sicht von Kotlin ist der Ablauf der gleiche.
Ob ich die Zahl 1 direkt in die Bedingung der Verzweigung schreibe,
oder stattdessen einen benannten Wert verwende,
der immer den Wert 1 haben wird, ist egal.

Wichtig hierbei: Das Gleichheitszeichen `=` kann verschiedene Bedeutungen haben.
In den meisten Fällen nutzen wir es,
wenn wir einen Wert unter einem Namen abspeichern wollen.
In Verzweigungen wird es aber oft genutzt,
um zu prüfen, ob zwei Werte gleich sind.
In dem Fall wollen wir vergleichen und nichts zuweisen oder abspeichern.
Um das klarzumachen, wird dann ein doppeltes Gleichheitszeichen verwende (`==`).

## Neuen Spielstand berechnen

Wie genau die Funktionen `menschlicherZug` und `computerZug` umgesetzt werden,
das können wir wieder auf später verschieben.
In unserer Funktion `runde` können wir an dieser Stelle erstmal davon ausgehen,
dass uns auf irgendeine Weise die Anzahl gezogener Hölzer zur Verfügung steht.
Damit ist die Zug-Phase abgeschlossen,
wir können jetzt also den Zustand des Spiels aktualisieren.
Das ist in unserem Fall eine simple Rechnung:
Die neue Anzahl Hölzer entspricht der alten Anzahl minus der gezogenen Anzahl.
In komplexeren Spielen wären die Berechnungen natürlich aufwendiger.

```
// Wie viele Hölzer sind jetzt noch übrig?
val neueAnzahlHoelzer = anzahlHoelzer - gezogeneHoelzer
```

## Siegbedingungen prüfen

Zum Abschluss der Runde müssen wir prüfen,
ob das Spiel beendet ist, oder ob es eine nächste Runde geben wird.
Wir müssen also prüfen,
ob der aktuelle Spieler die Bedingungen für einen Sieg erreicht hat.
Auch das ist bei unserem Spiel nicht allzu kompliziert:
Wir müssen dafür nur prüfen, ob noch mindestens ein Hölzchen übrig ist.
Wenn ja, geht das Spiel weiter,
wenn nicht, hat der aktuelle Spieler offenbar gerade das letzte Hölzchen genommen
und somit das Spiel gewonnen.

Auch hier brauchen wir also eine Verzweigung.
Die zu prüfende Bedingung dafür lässt sich wieder intuitiv formulieren:

```
// Ist das Spiel vorbei?
if (neueAnzahlHoelzer > 0) {
    // Es sind noch Hölzer vorhanden, das Spiel geht also weiter
    ...
} else {
    // Es sind keine Hölzer übrig, das Spiel ist vorbei!
    ...
}
```

Wenn das Spiel vorbei ist,
müssen wir nicht mehr viel machen.
Wir müssen nur ein letztes Mal einen Text ausgeben,
der darüber informiert, dass das Spiel nun vorbei ist.

## Nächste Runde einleiten

Interessanter ist der Programmablauf,
wenn das Spiel noch nicht beendet ist.
Dann müssen wir alle Informationen bereitstellen,
die unser Programm für die Durchführung der nächsten Runde benötigt.
Wie wir an der entwickelten Signatur der Funktion `runde` sehen können,
ist das die Anzahl der Hölzer und der aktuelle Spieler.
Die neue Anzahl Hölzer haben wir bereits berechnet
und unter dem Namen `neueAnzahlHoelzer` abgespeichert.

Um zu "berechnen", welcher Spieler in der nächsten Runde dran ist,
brauchen wir wieder eine Verzweigung.
Und die sieht der sehr ähnlich, die wir zuvor verwendet haben.
Denn wer in der nächsten Runde seinen Zug machen darf,
hängt davon ab, wer es in dieser Runde durfte.
Der Code dafür sieht dann so aus:

```
// Wer ist der nächste Spieler?
val naechsterSpieler: Int
if (aktuellerSpieler == mensch) {
    naechsterSpieler = computer
} else {
    naechsterSpieler = mensch
}
```

Auch hier lässt sich der Code mehr oder weniger wie ein deutscher Satz lesen:
"Wenn aktueller Spieler gleich Mensch, dann nächster Spieler gleich Computer" –
klingt logisch (wenn auch etwas gestelzt).

Was machen wir jetzt mit diesen beiden Informationen?
Wir wollen, dass die nächste Runde mit diesen beiden Ausgangswerten abläuft.
Für den Ablauf einer Runde haben wir eine Funktion, nämlich: `runde`.
Also müssen wir nichts weiter tun,
als diese Funktion aufzurufen und ihr die beiden neuen Werte als Eingaben mitzugeben.
Der letzte Teil unserer Funktion sieht also so aus:

```
// Ist das Spiel vorbei?
if (neueAnzahlHoelzer > 0) {

    // Wer ist der nächste Spieler?
    val naechsterSpieler: Int
    if (aktuellerSpieler == mensch) {
        naechsterSpieler = computer
    } else {
        naechsterSpieler = mensch
    }

    // Läute die nächste Runde ein!
    runde(neueAnzahlHoelzer, naechsterSpieler)

} else {

    // Informationen über das Spielende ausgeben
    println("Das Spiel ist vorbei!")
    if (aktuellerSpieler == mensch) {
        println("Der Mensch gewinnt!")
    } else {
        println("Der Computer gewinnt!")
    }

    // Eine nächste Runde gibt es nicht.
    // Die Programmausführung endet an dieser Stelle.
}
```

Funktionen können sich selbst aufrufen.
In diesem Fall beginnt Kotlin dann einfach eine neue Ausführung derselben Funktion,
beginnend bei der ersten Zeile der Funktion,
mit den neuen Eingabewerten.
Dieses Prinzip nennt sich auch Rekursion.
Es lässt sich für viele Programmieraufgaben nutzen
und ist oft ein guter Ansatz,
um Probleme in isolierte Teilprobleme zu zerlegen.

## Züge des Menschen und des Computers

Was nun noch fehlt,
sind die Funktionen `menschlicherZug` und `computerZug`.
Wenn der Mensch an der Reihe ist,
soll das Programm die Möglichkeit bieten, eine Zahl einzutippen.
Das kennen wir ja schon aus vorherigen Programmen.
Hier können wir also die gleichen Schritte mit der Funktion `readln`
und der Umwandlung in eine Zahl wieder nutzen.

Interessanter ist die Frage,
wie unser Computer Züge machen kann.
Dafür gibt es selbst bei einem so simplen Spiel wie diesem hier zahllose Möglichkeiten,
und wir wollen mehrere davon ausprobieren.
Zunächst können wir aber ganz simpel anfangen,
ohne Verwendung irgendeiner Logik,
die man als "künstliche Intelligenz" bezeichnen könnte.
Der einfachste mögliche Computerspieler ist der,
der einfach immer ein Hölzchen zieht, wenn er an der Reihe ist.

```
fun computerZug(): Int {
    return 1
}
```

Das ist nicht sehr beeindruckend.
Aber der Computerspieler erfüllt die wichtigsten Bedingungen:
Er wird immer eine Antwort geben, wenn er nach seinem Zug gefragt wird.
In anderen Szenarien könnte es sein,
dass der Computer keinen Zug ermitteln kann,
oder bei der Berechnung abstürzt, oder viel zu lange braucht.
Und: Der Zug, den der Computerspieler zurückgibt, wird immer ein gültiger Zug sein.
Wenn das Spiel noch nicht zu Ende ist,
wird immer mindestens ein Hölzchen auf dem Tisch liegen.
Auch das muss in anderen Szenarien nicht gegeben sein.
Wenn wir bei der Programmierung Fehler machen,
kann es sein, dass unser Computerspieler einen Zug wählt,
der gar nicht möglich oder erlaubt ist.
Als erste Version unseres Computerspielers ist dieses Programm also
gar nicht so schlecht,
wie man denken könnte.
Es kommt nur auf die Perspektive an.

Den vollständigen Code des Spiels findet ihr in der Datei
[NimSpiel.kt](NimSpiel.kt).

In der nächsten Woche werden wir versuchen,
den Computerspieler zu verbessern!
package week7

fun main() {

    val arrayVonZahlen: Array<Int> = arrayOf(4, 1234, 13, 12, 3141, 151)
    val liste = arrayVonZahlen.toList()
    println("Mein Array enthält folgende Werte: $liste")

    val wertAnDritterStelle = arrayVonZahlen[2]
    println("An dritter Stelle steht der Wert $wertAnDritterStelle")

    arrayVonZahlen[2] = 29

    println("Mein Array enthält folgende Werte: ${arrayVonZahlen.toList()}")
    val neuerWertAnDritterStelle = arrayVonZahlen[2]
    println("An dritter Stelle steht jetzt der Wert $neuerWertAnDritterStelle")

    val zweiDimensionalerArray: Array<Array<Int>> =
        arrayOf(
            arrayOf(23, 45, 12, 25),
            arrayOf(12, 41, 11, 95),
            arrayOf(14, 12, 86, 18),
            arrayOf(81, 27, 18, 90),
        )

    val dritteZeileVierteSpalte = zweiDimensionalerArray[2][3]
    println("In der dritten Zeile, vierte Spalte steht die Zahl $dritteZeileVierteSpalte")

    zweiDimensionalerArray[2][3] = 45
    val neuerWert = zweiDimensionalerArray[2][3]
    println("In der dritten Zeile, vierte Spalte steht die Zahl $neuerWert")
}

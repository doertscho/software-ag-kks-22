package week7

fun main() {
    val zahl: Int? = 23
    val andereZahl: Int? = 45
    val nochEineZahl: Int? = null

    // null ist nicht gleich 0

    if (zahl != null) {
        val dasDoppelteVonZahl = verdopple(zahl)
    }
    val dasDoppelteVonNochEineZahl = verdopple(nochEineZahl ?: 12)
}
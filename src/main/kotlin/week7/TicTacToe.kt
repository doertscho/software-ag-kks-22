package week7

// Eigens definierter Datentyp: Umfasst alle gültigen Werte für TicTacToe-Symbole
enum class Symbol {
    X,
    O,
}

enum class ChessPieceType {
    Knight,
    Bishop,
    Pawn,
    Queen,
    King,
}

fun verdopple(zahl: Int): Int {
    return zahl * 2
}

fun main() {

    //   X  O  X
    //   O  X  O
    //   O     O

    val spielfeld: Array<Array<Symbol?>> =
        arrayOf(
            arrayOf(Symbol.X, Symbol.O, Symbol.X),
            arrayOf(Symbol.O, Symbol.X, Symbol.O),
            arrayOf(Symbol.O, null,     Symbol.O),
        )
    spielfeld[1][2] = Symbol.X
}

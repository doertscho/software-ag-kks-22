package week1

fun verdopple(eingabe: Int): Int {
    val ergebnis = eingabe * 2
    return ergebnis
}

fun main() {
    println("Hello World!")

    val x = 25
    println("Die Zahl x ist:")
    println(x)

    val dasDoppelteVonX = verdopple(x)
    println("Das Doppelte von x ist:")
    println(dasDoppelteVonX)
}

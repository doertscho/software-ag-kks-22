# Einführung in die Programmierung mit Kotlin – Woche 3

## Erweiterung des Energie-Rechners

Nach dem gleichen Prinzip,
mit dem wir den Gesamtverbrauch pro Jahr berechnet haben,
können wir weitere Informationen berechnen
und unser Programm so noch nützlicher machen.
Im nächsten Schritt können wir beispielsweise
die zu erwartenden Stromkosten berechnen,
die die Benutzung des Geräts verursacht.

Hierzu müssen wir uns zunächst eine Frage stellen,
die sehr typisch für die Entwicklung von Software ist,
weil sie zeigt, dass es fast immer mehrere Möglichkeiten gibt,
ein Problem zu lösen.
Für die Berechnung des Gesamtstrompreises müssen wir wissen,
wie hoch der Preis für eine einzelne Verbrauchseinheit ist,
also in der Regel eine Kilowattstunde.
Woher bekommen wir diese Information?
Dafür gibt es mehrere Optionen:

* Wir können in dem Moment, in dem wir das Programm entwickeln,
  im Internet recherchieren, einen aktuellen Durchschnittspreis
  ermitteln, und diesen in unseren Programmcode schreiben.
* Wir können die Person vor dem Rechner auffordern,
  ihren aktuellen Strompreis einzutippen,
  so wie sie bereits Verbrauch und Nutzungsdauer eingetippt hat.
* Oder wir könnten (in einem komplexeren Programm) unseren Computer anweisen,
  diese Information selbstständig aus dem Internet abzurufen,
  zum Beispiel von der Website eines Stromanbieters
  oder aus einer öffentlichen Datenbank.

Alle Lösungen bieten Vor- und Nachteile.
Wenn wir einen Durchschnittspreis ermitteln und in unser Programm einbauen,
macht das die Benutzung für die Person leichter,
denn sie muss nicht eine weitere Information selbst eintippen.
Eventuell weiß die Person sogar gar nicht,
wie viel sie für ihren Strom bezahlt,
unser Programm könnte ihr also zusätzlichen Mehrwert bieten.
Andererseits kann der Strompreis je nach Anbieter und Wohnort variieren.
Und wenn jemand unser Programm in ein paar Jahren benutzt,
können sich die Preise inzwischen stark verändert haben
und unser Programm ist veraltet.

Wird der Strompreis von Hand eingetippt,
ist das Ergebnis vermutlich passender,
aber es ist eben auch zusätzlicher Aufwand für den Menschen
vor dem Bildschirm.

Die letzte Lösung bietet die Möglichkeit,
die Daten automatisch aktuell zu halten,
aber es ist auch die Lösung,
die einerseits den meisten Programmieraufwand erfordert
und bei der andererseits an vielen Stellen
Störungen und Probleme auftauchen können.
Was passiert, wenn unser Programm den Strompreis
von der Website eines Anbieters abrufen will,
aber diese Website zur Zeit nicht erreichbar ist?
Wenn unser Programm auf einem Rechner läuft,
der zur Zeit keine Internetverbindung hat?
Oder wenn der Stromanbieter sich irgendwann entscheidet,
diese Information nicht mehr öffentlich zur Verfügung zu stellen?

Ihr werden oft an solche Punkte gelangen,
an denen sich ein Problem auf mehrere Arten lösen lässt.
Die eine perfekte Lösung wird es dabei fast nie geben,
ihr müsst euch also entscheiden und versuchen,
zwischen Vor- und Nachteilen verschiedener Lösungen abzuwägen.

Wenn wir uns für die zweite Variante entscheiden
(also den Menschen den Preis pro Kilowattstunde selbst eintippen lassen),
könnte unser erweiterter Energie-Rechner so aussehen
wie in der Datei [EnergieRechnerErweitert.kt](EnergieRechnerErweitert.kt).

## Neuer Werte-Typ: Boolean für Wahrheitswerte

Wir kennen bereits Werte vom Typ `Int`
("integer", englisch für "ganze Zahl")
und Werte vom Typ `String` ("Zeichenkette", also Text).

Diese Woche verwenden wir einen weiteren Typ:
`Boolean` (benannt nach dem Mathematiker George Boole).
Boolean-Werte werden auch als Wahrheitswerte bezeichnet.
Es gibt nur zwei gültige Werte für diesen Typ:
`true` und `false` – wahr und falsch.
In den meisten Fällen lassen sie sich auch verstehen als
"ja" und "nein".

Solche Wahrheitswerte
werden uns in vielen verschiedenen Zusammenhängen begegnen.
Eines der simpelsten Beispiele sind auch hier Ausdrücke mit Zahlen.
Je nach Art der Formulierung können solche Ausdrücke
verschiedene Arten von Ergebnissen haben.
Wenn Kotlin beispielsweise den Ausdruck `8 + 5` auswertet,
dann führt es eine mathematische Berechnung durch,
und das Ergebnis ist ein Zahlenwert.
Wenn es hingegen den Ausdruck `8 > 5` auswertet,
dann wird überprüft,
ob die angegebene Aussage stimmt oder nicht.
Das Ergebnis ist entweder "ja" (die Aussage stimmt)
oder "nein" (die Aussage stimmt nicht),
es ist also ein solcher Boolean-Wert.
Den Ausdruck `8 > 5` würde Kotlin auswerten zu `true`,
den Ausdruck `5 > 8` würde Kotlin auswerten zu `false`.

So direkt würden wir das vermutlich nicht in unserem Programmcode
aufschreiben,
denn dass acht größer ist als fünf, wird immer gelten.
Das müssen wir nicht wieder und wieder von Kotlin berechnen lassen.
Sinnvoll werden solche Überprüfungen dann,
wenn ein Teil der Bedingung unvorhersehbar ist,
also zum Beispiel dann,
wenn unser Programm die Eingabe einer beliebigen Zahl zulässt.
Wenn wir diese zwischenspeichern mit `val zahl = liesZahl()`,
dann kann der Ausdruck `zahl > 5` mal zu `true` ausgewertet werden
und mal zu `false`,
je nachdem, welche Zahl eingetippt wurde.

Boolean-Werte spielen aber nicht nur bei Zahlen eine Rolle.
Wir werden viele weitere Beispiele in den folgenden Wochen kennenlernen.
Für Text-Daten bietet Kotlin beispielsweise vordefinierte Funktionen,
die Überprüfen, ob der Text ein bestimmtes Wort enthält.
Auch diese geben Boolean-Werte zurück.
In diesem Fall bedeutet die Rückgabe dann:
`true`, also ja, der Text beinhaltet das gesuchte Wort, oder
`false`, nein, der Text beinhaltet das gesuchte Wort nicht.

## Neue Struktur: Verzweigungen

Solche Boolean-Werte verwenden wir für Verzweigungen.
Eine Verzweigung ist ein Konstrukt,
mit dem ihr dafür sorgen könnt,
das bestimmte Anweisungen nur unter bestimmten Bedingungen ausgeführt werden.
In Kotlin-Programmen gibt es eine Trennung zwischen Daten und Anweisungen,
über Verzweigungen aber können die konkreten Daten Einfluss nehmen
auf den Ablauf des Programms.

Eine Verzweigung könnt ihr in eurem Programm mit dem Schlüsselwort `if` einleiten
(englisch für "falls").
Dann wird eine Bedingung definiert, die von konkreten Daten abhängt.
Ist diese bei der Ausführung des Codes erfüllt,
wird der folgende Block von Code ausgeführt.
Als einen "Block" bezeichnen wird eine Reihe von Anweisungen,
die jeweils durch geschweifte Klammern (`{` und `}`) umschlossen wird.
Ansonsten wird der Block nicht ausgeführt. Beispiel:
```
val zahl = liesZahl()
if (zahl > 5) {
  println("Unsere Zahl ist größer als fünf!")
}
```

Es kann anschließend ein alternativer Block von Code definiert werden,
der ausgeführt wird, wenn die angegebene Bedingung nicht erfüllt ist.
Diesem wird dann das Schlüsselwort `else` vorgestellt
(englisch für "andernfalls").

```
val zahl = liesZahl()
if (zahl < 0) {
  println("Unsere Zahl ist negativ, also kleiner als 0!")
} else {
  println("Unsere Zahl ist positiv!")
}
```

Solche Überprüfungen können ebenso durch Funktionsaufrufe definiert werden.
Diese müssen dann einen Boolean-Wert zurückgeben.
```
fun istPrimzahl(x: Int): Boolean {
  ???
}

val x = liesZahl()
if (istPrimzahl(x)) {
  println("Die Zahl $x ist eine Primzahl!")
}
```
# Einführung in die Programmierung mit Kotlin – Woche 5

## Weiterentwicklung der Strategie des Computer-Spielers

Unsere erste Version des Computer- bzw. "KI"-Spielers,
die wir in der letzten Woche programmiert haben,
hatte ja noch nicht allzu viel "Intelligenz" in sich.
Sie war aber trotzdem ein guter Anfang,
denn sie war in der Lage, einen gültigen Zug zu machen.
Das ist beim Nim-Spiel natürlich auch nicht sonderlich schwer,
aber in komplexeren Spielen kann es durchaus sein,
dass dieser erste Schritt schon gar nicht so leicht zu erreichen ist.

Ausgehend davon können wir nun die Logik unseres Computer-Spielers
schrittweise verbessern.
Wie genau das aussieht, das wird immer sehr stark von der Art des Spiels abhängen.
In allen Fällen wird es hilfreich sein,
das Spiel selbst ein paar Mal zu spielen.
Versucht, Muster zu erkennen.
Meistens wird man als Mensch bei einem Spiel intuitiv einer gewissen Strategie folgen.
Diese aber in klaren Worten zu formulieren,
die wir als Grundlage für die Entwicklung eines Programms nutzen können,
ist oft gar nicht so leicht.

## Gewinn-Züge erkennen

Ein sinnvoller erster Schritt dürfte aber in fast allen Spielen nützlich sein.
Unser Computer-Spieler sollte in der Lage sein, Situationen zu erkennen,
in denen er mit dem nächsten Zug das Spiel gewinnen kann.
In unserem Beispiel (oder Bei-Spiel 🥁) bedeutet das:
Wenn nur noch eines, zwei oder drei Hölzchen auf dem Tisch liegen,
sollten wir erkennen,
dass wir sofort gewinnen, wenn wir einfach alle verbliebenen Hölzchen nehmen.
Die erste Version unseres Spielers würde ja auch dann nur ein Hölzchen nehmen,
wenn nur noch zwei oder drei Hölzchen auf dem Tisch liegen,
und somit dem Gegner den Sieg ermöglichen.

Um die Situation erkennen zu können,
braucht der Computer-Spieler natürlich Informationen über den aktuellen Zustand
des Spiels,
in diesem Fall also über die Anzahl der Hölzchen.
Das iat ja auch nur fair – wir als Menschen, die das Spiel spielen,
haben diese Information ja auch zur Verfügung,
und richten unsere Züge danach aus.
Das Spiel muss die Anzahl der Hölzchen also als Eingabewert
an die Funktion übergeben,
die die Logik unseres Spielers abbildet.
Dann können wir mit einer Verzweigung versuchen,
eine Gewinn-Situation zu erkennen.
Wenn wir uns nicht in einer solchen Gewinn-Situation befinden,
also noch mehr als drei Hölzchen auf dem Tisch liegen,
dann nutzen wir einfach die "Logik" der ersten Version unseres Spielers.

```
// Etwas schlauerer Computerspieler. Wenn er das Spiel beenden kann, macht er es.
fun computerZugVersion2(anzahlHoelzer: Int): Int {
    if (anzahlHoelzer < 4) {
        // Verhalten in einer Gewinn-Situation
        return anzahlHoelzer
    } else {
        // Standardverhalten (übernommen von der vorherigen Version)
        return 1
    }
}
```

## In die Zukunft schauen

Unser Computer-Spieler kann nun Gewinn-Situationen erkennen.
Wenn er das kann, dann kann er auch in die Zukunft schauen.
Die nächste sinnvolle Verbesserung besteht darin,
dass unsere Logik prüft,
welche Situation ein möglicher Zug für den Gegner hinterlässt.
Wir kennen ja die Regeln des Spiels und können daher voraussagen,
welche Auswirkungen auf den Spielzustand ein möglicher Zug von uns haben wird.

Konkret bedeutet das beim Nim-Spiel:
Wir sollten vermeiden,
unserem Gegner eine Restanzahl von einem, zwei oder drei Hölzchen zu überlassen.
Manchmal haben wir keine Wahl:
Wenn noch vier Hölzchen auf dem Tisch liegen,
und wir unseren Zug machen müssen,
dann ist die Sache aussichtslos.
Egal, ob wir ein, zwei oder drei Hölzchen nehmen,
unser Gegner wird danach die Gelegenheit haben, das Spiel zu gewinnen.
Wenn aber fünf Hölzchen vor uns liegen,
können wir eine Wahl treffen:
Ziehen wir zwei oder drei Hölzchen,
wird unser Gegner im nächsten Zug gewinnen.
Ziehen wir aber nur eines, kann der Gegner das Spiel nicht beenden.
Mehr noch: Unser Gegner wird sich dann in der aussichtslosen Situation befinden,
in der jeder mögliche Zug wiederum uns den Sieg ermöglicht.

Noch vielfältiger sind die Optionen, wenn sechs Hölzchen vor uns liegen.
Ziehen wir drei, ist der Sieg für unseren Gegner gewiss.
(Es sei denn vielleicht, unser Gegner ist die Version 1 unseres Computer-Spielers,
die einfach immer nur ein Hölzchen nimmt.)
Ziehen wir zwei, hinterlassen wir unserem Gegner vier Hölzchen
und bringen ihn damit in die Zwickmühle.
Ziehen wir nur eines, hat unser Gegner wiederum mehrere Optionen.
Wenn er schlau genug ist, kann er dann uns in die Situation bringen,
dass wir das Spiel nicht mehr gewinnen können.
Alle drei möglichen Züge für uns führen also zu unterschiedlichen Situationen.

In einer dritten Version unseres Computer-Spielers könnten wir diese Gedanken
wie folgt in Programmcode umsetzen:

```
// Noch schlauerer Computerspieler. Versucht zu verhindern,
// das der Gegner das Spiel gewinnen kann.
fun computerZugVersion3(anzahlHoelzer: Int): Int {
    if (anzahlHoelzer < 4) {
        return anzahlHoelzer
    } else {
        if (anzahlHoelzer == 5) {
            return 1
        } else {
            if (anzahlHoelzer == 6) {
                return 2
            } else {
                // Nachdem wir alle bekannten Sonderfälle geprüft haben,
                // und keiner davon eingetreten ist,
                // nehmen wir wieder das Standardverhalten.
                return 1
            }
        }
    }
}
```

Bei alledem müssen wir stets bedenken:
Auch wenn manche Sachen für uns offensichtlich erscheinen,
der Computer wird immer nur das tun,
das wir ihm vorher ausdrücklich beigebracht haben.
Wenn wir uns Strategien überlegen,
müssen wir uns also jeden noch so kleinen Aspekt daran bewusst machen
und ihn in unserem Programmcode berücksichtigen.

## Vereinfachte Schreibweise für größere Verzweigungen

In dem obigen Code haben wir mehrere Bedingungen,
die wir prüfen,
und müssen darum mehrere Verzweigungen ineinander verschachteln.
Das kann schnell etwas unübersichtlich werden.
In Fällen wie diesen,
in denen im `else`-Block sofort das nächste `if` folgt,
kann man den Programmcode aber etwas übersichtlicher aufschreiben.
Das sieht dann so aus:

```
// Noch schlauerer Computerspieler. Versucht zu verhindern,
// das der Gegner das Spiel gewinnen kann.
fun computerZugVersion3(anzahlHoelzer: Int): Int {
    if (anzahlHoelzer < 4) {
        return anzahlHoelzer
    } else if (anzahlHoelzer == 5) {
        return 1
    } else if (anzahlHoelzer == 6) {
        return 2
    } else {
        // Standardverhalten
        return 1
    }
}
```

Das sieht übersichtlicher aus, oder?
Beide Varianten funktionieren aber gleichermaßen.
Und es gibt in diesem fall sogar eine dritte mögliche Schreibweise
für den gleichen Code.
Das liegt daran, dass der Computer ja sofort aus der Funktion herausspringt,
sobald er das Stichwort `return` sieht.
Der restliche Code in der aktuellen Funktion wird dann ignoriert.
Darum ist auch der folgende Code äquivalent:

```
// Noch schlauerer Computerspieler. Versucht zu verhindern,
// das der Gegner das Spiel gewinnen kann.
fun computerZugVersion3(anzahlHoelzer: Int): Int {
    if (anzahlHoelzer < 4) {
        return anzahlHoelzer
    }
    if (anzahlHoelzer == 5) {
        return 1
    }
    if (anzahlHoelzer == 6) {
        return 2
    }
    
    // Standardverhalten
    return 1
}
```

Aber Achtung, das funktioniert nur dann genau so,
wenn innerhalb des `if`-Blocks ein `return` steht.
Ich persönlich finde in diesem Fall die letzte Variante am übersichtlichsten.
Das ist aber wie so oft Geschmackssache,
und es ist euch überlassen,
wie ihr euren Code für euch am besten strukturieren könnt.

## Noch weiter in die Zukunft blicken

... Fortsetzung folgt!

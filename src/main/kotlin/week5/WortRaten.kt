package week5

fun runde(richtigesWort: String, anzahlVersuche: Int) {

    println("Bitte gib ein Wort ein:")
    val eingabe = readln()

    if (eingabe == richtigesWort) {
        println("Richtig! Du hast gewonnen. Du hast $anzahlVersuche Versuche gebraucht.")
    } else {
        val neueAnzahlVersuche = anzahlVersuche + 1
        runde(richtigesWort, neueAnzahlVersuche)
    }
}

fun main() {

    println("Herzlich willkommen beim Wort-Rate-Spiel.")

    val wort = "wortraten"
    runde(wort, 0)
}
package week5

import kotlin.random.Random

// Notizen:
// Gesamtanzahl Streichhölzer festlegen
// Zahl entgegennehmen (Mensch macht seinen Zug)
// Computer muss Zahl bestimmen (Computer macht seinen Zug)
// Neue Gesamtanzahl berechnen
// Wer hat gewonnen?
// Wurde das letzte Streichholz genommen?

val mensch = 1
val computer = 2

fun menschlicherZug(): Int {
    println("Du bist an der Reihe, bite gib ein, wie viele Streichhölzer du nehmen möchtest (1, 2 oder 3):")
    val eingabeText = readln()
    val eingabeZahl = eingabeText.toInt()
    return eingabeZahl
}

// Simpelster Computerspieler. Nimmt immer 1 Hölzchen.
// Aber: Immerhin macht er damit immer einen gültigen Zug!
fun computerZugVersion1(): Int {
    return 1
}

// Etwas schlauerer Computerspieler. Wenn er das Spiel beenden kann, macht er es.
fun computerZugVersion2(anzahlHoelzer: Int): Int {
    if (anzahlHoelzer < 4) {
        return anzahlHoelzer
    } else {
        return 1
    }
}

// Noch schlauerer Computerspieler. Versucht zu verhindern,
// das der Gegner das Spiel gewinnen kann.
fun computerZugVersion3(anzahlHoelzer: Int): Int {
    if (anzahlHoelzer < 4) {
        return anzahlHoelzer
    } else if (anzahlHoelzer == 5) {
        return 1
    } else if (anzahlHoelzer == 6) {
        return 2
    } else {
        return 1
    }
}

// Der schlauste Computerspieler. Spielt mit der idealen Strategie.
fun computerZugVersion4(anzahlHoelzer: Int): Int {
    val rest = anzahlHoelzer % 4
    if (rest == 0) {
        return Random.nextInt(1, 3)
    } else {
        return rest
    }
}

// Diese Funktion beschreibt den Ablauf einer einzelnen Spielrunde
fun runde(anzahlHoelzer: Int, aktuellerSpieler: Int) {

    // Zeige aktuellen Spielstand an
    println("Es liegen noch $anzahlHoelzer Streichhölzer auf dem Tisch.")

    // Wie viele Hölzer werden genommen?
    // Wie wir das herausfinden, hängt davon ab, wer gerade an der Reihe ist!
    val gezogeneHoelzer: Int
    if (aktuellerSpieler == mensch) {
        // Wenn der Mensch an der Reihe ist, dann ...
        gezogeneHoelzer = menschlicherZug()
    } else {
        // Wenn der Computer an der Reihe ist, dann ...
        gezogeneHoelzer = computerZugVersion4(anzahlHoelzer)
        println("Der Computer nimmt $gezogeneHoelzer Hölzchen.")
    }

    // überprüfe auf gültige Eingabe
    if (gezogeneHoelzer > 3) {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }
    if (gezogeneHoelzer < 1) {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }
    if (gezogeneHoelzer > anzahlHoelzer) {
        error("Ungültige Eingabe! So viele Hölzer gibt es gar nicht mehr. Du wirst disqualifiziert.")
    }

    // Wie viele Hölzer sind jetzt noch übrig?
    val neueAnzahlHoelzer = anzahlHoelzer - gezogeneHoelzer

    // Ist das Spiel vorbei?
    if (neueAnzahlHoelzer > 0) {

        // Wer ist der nächste Spieler?
        val naechsterSpieler: Int
        if (aktuellerSpieler == mensch) {
            naechsterSpieler = computer
        } else {
            naechsterSpieler = mensch
        }

        runde(neueAnzahlHoelzer, naechsterSpieler)

    } else {
        // Informationen über das Spielende ausgeben
        println("Das Spiel ist vorbei!")
        if (aktuellerSpieler == mensch) {
            println("Der Mensch gewinnt!")
        } else {
            println("Der Computer gewinnt!")
        }
    }
}

fun main() {

    println("Herzlich willkommen beim Nim-Spiel!")

    val startAnzahl = 21
    val startSpieler = mensch

    // Beginne die erste Runde
    runde(startAnzahl, startSpieler)

}

package week8.playground

// Eigens definierter Datentyp: Umfasst alle gültigen Werte für TicTacToe-Symbole
enum class Symbol {
    X,
    O,
}

val board: Array<Array<Symbol?>> =
    arrayOf(
        arrayOf(null, null, null),
        arrayOf(null, null, null),
        arrayOf(null, null, null),
    )

val nextPlayer: Symbol = Symbol.X

data class Move(
    val row: Int,
    val col: Int,
)

fun nextComputerMove(board: Array<Array<Symbol?>>, mySymbol: Symbol): Move {

    return Move(0, 2)
}

interface Player {
    fun nextMove(board: Array<Array<Symbol?>>, mySymbol: Symbol): Move
}

class DumbPlayer : Player {

    override fun nextMove(board: Array<Array<Symbol?>>, mySymbol: Symbol): Move {
        return Move(0, 2)
    }

}

fun main() {

    val move: Move = nextComputerMove(board, nextPlayer)
    val row: Int = move.row
    val col: Int = move.col

    // Prüfe, ob die Koordinaten gültig sind:
    if (row < 0) {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }
    if (row > 2) {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }
    if (col < 0) {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }
    if (col > 2) {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }

    // Prüfe, ob das Feld frei ist:
    if (board[row][col] == null) {
        board[move.row][move.col] = nextPlayer
    } else {
        error("Ungültige Eingabe! Du wirst disqualifiziert.")
    }

    // Prüfe, ob der aktuelle Spieler mit seinem Zug das Spiel gewonnen hat

    // Prüfe, ob das Spiel beendet ist, weil alle Felder besetzt wurden

    // Gib dem nächsten Spieler die Möglichkeit, seinen Zug zu machen
}

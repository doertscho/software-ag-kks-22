package week8.ui

import javafx.application.Platform
import javafx.stage.Stage
import tornadofx.*
import week8.Player
import week8.State
import week8.Symbol
import week8.ai.DumbPlayer
import week8.ai.FieldCheckingPlayer
import week8.ai.SimpleAiPlayer
import week8.ai.WinningMoveAiPlayer
import week8.runGame

class TicTacToeUI : App(MainView::class) {

    private val stateContainer: StateContainer by inject()
    private val uiPlayer: UiPlayer by inject()

    override fun start(stage: Stage) {
        super.start(stage)

        val players: Map<Symbol, Player> = mapOf(
            Symbol.O to uiPlayer,
            Symbol.X to FieldCheckingPlayer(),
        )

        Platform.runLater {
            runGame(stateContainer.state, players)
        }
    }
}

class StateContainer : Component(), ScopedInstance {
    val state = State()
}

fun main() {
    launch<TicTacToeUI>()
}























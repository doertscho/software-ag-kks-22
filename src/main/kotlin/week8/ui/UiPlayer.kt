package week8.ui

import javafx.beans.property.SimpleObjectProperty
import tornadofx.Component
import tornadofx.ScopedInstance
import tornadofx.awaitUntil
import week8.Move
import week8.Player
import week8.State

class UiPlayer : Player, Component(), ScopedInstance {

    private val clicked = SimpleObjectProperty<Move?>(null)

    fun click(rowNo: Int, columnNo: Int) {
        clicked.set(Move(rowNo, columnNo))
    }

    override fun nextMove(state: State): Move {
        clicked.awaitUntil { it != null }
        val nextMove = clicked.value ?: error("failed to retrieve click")
        clicked.set(null)
        return nextMove
    }
}

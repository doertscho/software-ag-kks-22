package week8.ai

import week8.Move
import week8.Player
import week8.State

class DumbPlayer : Player {
    override fun nextMove(state: State): Move {
        return Move(0, 2)
    }
}

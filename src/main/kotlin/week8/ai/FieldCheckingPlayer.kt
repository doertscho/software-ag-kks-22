package week8.ai

import week8.Move
import week8.Player
import week8.State

class FieldCheckingPlayer : Player {

    override fun nextMove(state: State): Move {
        val board = state.field

        for (row in 0 .. 2) {
            for (column in 0 .. 2) {
                if (board[row][column] == null) {
                    return Move(row, column)
                }
            }
        }

        error("impossible state")
    }
}
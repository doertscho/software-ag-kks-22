package week8.ai

import week8.Move
import week8.Player
import week8.State
import week8.isValidMove

class SimpleAiPlayer : Player {

    override fun nextMove(state: State): Move {
        for (row in 0 .. 2) {
            for (column in 0 ..2) {
                if (isValidMove(state, Move(row, column))) {
                    return Move(row, column)
                }
            }
        }
        error("impossible situation detected!")
    }
}

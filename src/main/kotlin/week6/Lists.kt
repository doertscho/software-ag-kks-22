package week6

fun meineFunktion(eingabeWert: String) {

}

fun main() {

    val text: String = "Hallo Welt!"
    val zahl: Int = 4

    val float: Float = 3.14f

    val listeVonZahlen: List<Int> = listOf(4, 123, 4, 4, 12, 4, 4, 51, 12, 5151, 145890, 141, 1411)
    val anzahlVonElementen: Int = listeVonZahlen.size
    println("Meine Liste von Zahlen enthält $anzahlVonElementen Einträge!")

    if (listeVonZahlen.contains(513)) {
        println("Die List enthält die Zahl 513!")
    } else {
        println("513 wurde nicht in der Liste gefunden.")
    }

    val groessteZahl: Int = listeVonZahlen.max()
    println("Die größte Zahl in der Liste ist die $groessteZahl")

    val listeVonZahlenOhneDuplikate: List<Int> = listeVonZahlen.distinct()
    println("Die Liste ohne Duplikate enthält: $listeVonZahlenOhneDuplikate")

    val listeVonGeradenZahlen: List<Int> = listeVonZahlen.filter(::istGerade)
    println("Wenn wir nur die geraden Zahlen behalten, lautet die Liste: $listeVonGeradenZahlen")

    val listeVonUngeradenZahlen: List<Int> = listeVonZahlen.filter {
        val rest = it % 2
        rest == 1
    }

    println("Bitte gib ein paar Wort ein, getrennt durch Komma:")
    val eingabeText: String = readln()
    val listeVonWorten: List<String> = eingabeText.split(",")
    println("Die eingegeben Worte sind: $listeVonWorten")
    val anzahlVonWorten: Int = listeVonWorten.size
    println("Du hast $anzahlVonWorten Worte eingegeben!")
    // hallo,welt,wie,geht,es,dir
}

fun istGerade(zahl: Int): Boolean {
    val rest = zahl % 2
    return rest == 0
}

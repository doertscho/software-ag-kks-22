# Einführung in die Programmierung mit Kotlin – Woche 2

## Mehrstufige Programme entwickeln

Eine Sichtweise auf das Programmieren,
die ich sehr passend finde, ist die folgende:
Ein Problem oder eine Aufgabe wird in Teilschritte zerlegt,
die jeweils ein neues (Teil-)Problem oder eine neue (Teil-)Aufgabe darstellen.
Diese werden wiederum in Teilschritte zerlegt, und immer so weiter,
bis wir auf einem Niveau angekommen sind,
auf dem wir dem Computer mit den uns bekannten vordefinierten Funktionen
sagen können,
wie er ein konkretes Teilproblem zu lösen hat.

So gehe auch ich bei der Entwicklung eines Programms vor.
Ich betrachte ein Problem zunächst auf hoher Ebene
und schreibe Programmcode für zwei bis fünf Teilschritte auf.
Diese Teilschritte formuliere ich aber nicht vollständig aus,
sondern notiere einfach einen Aufruf einer Funktion,
die das macht, was ich brauche – die es aber noch gar nicht gibt.
Ich weiß aber, welche Eingabe- und Ausgabedaten
eine solche Funktion haben wird.
Sobald ich die Abfolge auf oberster Ebene fertig aufgeschrieben habe,
kann ich mich dann daran machen,
all die fehlenden Funktionen konkret aufzuschreiben.

Mit diesem Gedanken wollen wir nun ein erstes Programm entwickeln,
das mehrere Arbeitsschritte umfasst.
Das Ergebnis soll ein Energie-Rechner sein.
Als Benutzer*in des Programms sollen wir zunächst die Möglichkeit haben,
zwei Zahlen einzugeben:
den Stromverbrauch eines Elektrogeräts und die tägliche Nutzungsdauer.
Daraus soll unser Programm berechnen,
wie viel Energie das Gerät so in einem Jahr verbraucht,
welche Kosten entstehen, und wie hoch die CO2-Emissionen sind.

## Eingaben während der Ausführung des Programms einlesen

Zunächst wollen wir zwei Eingaben vom Menschen einlesen.
Es handelt sich dabei jeweils um Zahlen.
Das können wir zunächst genau so aufschreiben.
Um das Programm etwas besser verständlich zu machen,
zeigen wir vorher jeweils etwas Text an,
der erklärt, was für eine Eingabe erwartet wird.
Die eingegebenen Zahlen wollen wir uns jeweils 
als benannte Werte zwischenspeichern.

```
fun main() {

    println("Hallo! Bitte gib den Stromverbrauch des Geräts ein [in Watt]:")
    val stromverbrauchW = liesZahl()

    println("Bitte gib an, wie viele Stunden pro Tag das Gerät benutzt wird:")
    val stundenAmTag = liesZahl()
}
```

Diesen Code können wir so noch nicht ausführen,
denn die Funktion `liesZahl` kennt Kotlin nicht.
Wir müssen sie selbst definieren.
Hier zeigt sich aber schon ein Vorteil von selbst definierten Funktionen:
Wir müssen die gleiche Aufgabe (eine Zahl einlesen) mehrfach ausführen,
dafür aber die benötigten Schritte nur einmal aufschreiben.
Die selbst definierte Funktion können wir dann beliebig oft aufrufen.

Für die Definition einer eigenen Funktion
beginnen wir wieder mit der sogenannten Signatur.
Das heißt: Wir müssen uns zunächst wieder fragen,
was die Eingabedaten und die Rückgabedaten unserer Funktion sind.
Für die Rückgabe ist das in diesem Fall vielleicht einfacher:
Wir möchten einen Zahlenwert haben.
Für Zahlen kennen wir schon den Datentyp `Int`.
Das ist allerdings nur eine von mehreren Möglichkeiten,
Zahlen in Kotlin darzustellen.
Er ist beschränkt auf ganze Zahlen.
Ein flexiblerer Datentyp für Zahlen heißt `Double`
(das steht für "double precision floating point number",
also sinngemäß etwa "besonders präzise Fließkommazahl").
Mit diesem können (fast) beliebige Zahlen verarbeitet werden.

Welche Eingabedaten benötigt unsere Funktion `liesZahl`?
Das ist vielleicht schon weniger offensichtlich.
Die Antwort lautet: keine!
Denn: Wir müssen unterscheiden zwischen Eingabedaten,
die unser Programm für die Ausführung einer Funktion bereitstellen muss,
und Eingaben, die die Person vor dem Bildschirm über die Tastatur eintippt.
Erstere sind das, was in der Signatur einer Funktion auftaucht.
Eingaben "von außen" kann unser Programm ja im Vorwege nicht kennen.
Die Signatur unserer Funktion sieht also so aus:

```
fun liesZahl(): Double
```

Was hier nun passieren soll, ist,
dass das Programm pausiert und wartet,
bis wir eine Eingabe getätigt haben.
Dann wird der Programmablauf fortgesetzt und der eingegebene Wert
kann verarbeitet werden.
Hierfür können wir eine weitere vordefinierte Kotlin-Funktion nutzen:
`readln()`.
Wie der Name schon andeutet, 
ist diese gewissermaßen das Gegenstück zu `println()`:
Über die Funktion `println` kann unser Programm Text an uns senden
(der dann auf dem Bildschirm angezeigt wird),
über `readln` können wir Text an unser Programm senden.

Wohlgemerkt: Wir senden *Text* an unser Programm.
Wenn wir die Funktion `readln` verwenden,
und wir während des Programmablaufs etwas eintippen,
dann weiß Kotlin an dieser Stelle nichts davon,
dass wir in diesem Fall eine zahl eintippen wollen.
Das zeigt auch ein Blick auf die Signatur der Funktion `readln`.
Vordefinierte Funktionen haben genau so eine Signatur
wie unsere selbstgeschriebenen,
also eine Definition,
welche Eingabedaten sie benötigen und welche Rückgabedaten sie liefern.
Wenn wir IntelliJ IDEA verwenden,
wird uns die Signatur beispielsweise dann angezeigt,
wenn wir mit der Maus über das Wort `readln` fahren.
Die Signatur von `readln` sieht wie folgt aus:

```
fun readln(): String
``` 

Der Datentyp `String` 
(von Englisch "string" für "Kette", in diesem Fall: "Zeichenkette")
steht für beliebigen Text.
Wenn wir also die Funktion `readln` aufrufen
und uns ihren Rückgabewert speichern,
haben wir einen Text-Wert.

📝 **Zum Merken:** Der Datentyp `String` steht für Text,
der Datentyp `Double` für Zahlen.

## Umwandlung von Datentypen

Unsere Funktion `liesZahl` soll aber einen Zahlwert zurückgeben.
Darum müssen wir Kotlin an dieser Stelle ausdrücklich sagen,
dass es versuchen soll, die Eingabe in einen Zahlwert umzuwandeln.
Dafür gibt es zum Glück auch eine vordefinierte Funktion.
Die heißt `toDouble()`, 
passend zum Namen des gewünschten Datentyps, `Double`.
Unsere Funktion `liesZahl` kann damit so aussehen:

```
fun liesZahl(): Double {
    val eingabeText = readln()
    val eingabeZahl = eingabeText.toDouble()
    return eingabeZahl
}
```

Was hier auffällt: Der Aufruf der Funktion `toDouble`
sieht anders aus als bisherige Funktionsaufrufe.
In Kotlin gibt es zwei verschiedene Möglichkeiten,
Funktionsaufrufe zu notieren,
und es hängt von der Art der Funktion ab, welche wir verwenden.
Wenn Funktionen im Kontext eines bestimmten Datentyps stehen,
dann werden ihre Aufrufe auf diese Art und Weise notiert:
Zunächst schreiben wir den Wert,
den wir verarbeiten wollen, dann folgt ein Punkt,
und dann der Name der Funktion.
Wann genau das der Fall ist,
müsst ihr an dieser Stelle noch nicht wirklich verstehen.
Tatsächlich werden die meisten vordefinierten Funktionen
auf diese Art und Weise aufgerufen.
Funktionen wie `println` und `readln` sind hier eher die Ausnahme.
Wir werden in den nächsten Wochen noch viele weitere Beispielen kennen lernen.

📝 **Zum Merken:** Viele Funktionen werden mit der Punkt-Notation aufgerufen.

Auch hier gibt es eine praktische Funktion von IntelliJ IDEA,
die uns beim Programmieren unterstützt:
Wenn wir in unserem Programmcode einen Wert aufschreiben
und direkt dahinter einen Punkt,
öffnet sich eine lange Liste mit Vorschlägen,
die alle zur Verfügung stehenden Funktionen zeigt,
die wir an dieser Stelle verwenden können.
Alle vordefinierten Funktionen tragen englische Namen,
wenn ihr also wisst, was ihr machen wollt,
besteht oft die Möglichkeit, den Namen einer Funktion zu erraten.
IntelliJ IDEA wird euch dann wiederum weitere Informationen anzeigen,
und ihr könnt überprüfen,
ob die Funktion tatsächlich das tut, was ihr erwartet.

## Verständliche Namen

Zur Erinnerung: Die Namen meiner zwischengespeicherten Werte
(`eingabeText` und `eingabeZahl`)
kann ich völlig frei wählen.
Aus Sicht des Computers ist es völlig egal,
was dort steht, solange ich denselben Namen wieder genau so verwende,
wenn ich an späterer Stelle wieder auf den Wert zurückgreifen möchte.
Ich könnte die Funktion auch so aufschreiben,
wenn ich wollte:

```
fun liesZahl(): Double {
    val karlheinz = readln()
    val manfred = karlheinz.toDouble()
    return manfred
}
```

Auch das hier würde der Computer ausführen, ohne zu meckern:

```
fun liesZahl(): Double {
    val asdfkjl124Hafsd1f = readln()
    val ag8adfg0980jl12kljkg = asdfkjl124Hafsd1f.toDouble()
    return ag8adfg0980jl12kljkg
}
```

Aber gut gewählte Namen helfen mir und anderen Menschen,
meinen Code zu verstehen.
In diesem Fall dient der Namen `eingabeText` als Erinnerung daran,
dass die Funktion `readln` mir einen Text-Wert liefert,
den ich in einem nächsten Schritt noch in eine Zahl umwandeln muss.

## Weitere einfache Rechenoperationen

Wir haben nun alles, was wir brauchen,
um unsere beiden Ausgangsinformationen während des Programmablaufs einzulesen.
Damit unser Programm auch einen Zweck hat,
müssen wir diese nun verarbeiten
und so neue Daten, neue Informationen generieren.
Im ersten Schritt soll unser Programm berechnen,
wie viel Energie das Gerät insgesamt in einem Jahr verbraucht
(in Kilowattstunden, abgekürzt kWh).

Das können wir zunächst wieder in unserer Hauptfunktion aufschreiben,
ohne uns darüber Gedanken zu machen, wie genau diese Berechnung denn aussieht,
denn das lagern wir aus in eine weitere Funktion.

```
fun main() {

    println("Hallo! Bitte gib den Stromverbrauch des Geräts ein [in Watt]:")
    val stromverbrauchW = liesZahl()

    println("Bitte gib an, wie viele Stunden pro Tag das Gerät benutzt wird:")
    val stundenAmTag = liesZahl()
    
    val verbrauchProJahrKWH = berechneVerbrauchProJahrKWH()
    println("Der Energieverbrauch pro Jahr beträgt [in kWh]:")
    println(verbrauchProJahrKWH)
}
```

Unsere neue Funktion `berechneVerbrauchProJahrKWH`
soll uns einen Zahlwert liefern,
der Rückgabe-Datentyp ist hier also wieder `Double`.
Um den Verbrauch berechnen zu können,
braucht sie die beiden Zahlen, die wir zuvor eingelesen haben.
Das sind also ihre Eingabedaten,
die wir in der Signatur der Funktion innerhalb der Klammern definieren.
Die Signatur unserer Funktion sieht also so aus:

```
fun berechneStromverbrauchProJahrKWH(stromverbrauchW: Double, stundenAmTag: Double): Double
```

Die Berechnung des Jahresverbrauchs erfolgt in mehreren Schritten:
Wir berechnen zunächst den Tagesverbrauch,
dann multiplizieren wir diesen mit 365, um den Jahresverbrauch zu erhalten.
Schließlich können wir noch die Einheit des Wertes anpassen:
Wir geben den Stromverbrauch in Watt (W) an, nicht Kilowatt (kW),
die gängige Einheit für die Abrechnung des Stromverbrauchs
ist aber Kilowattstunden (kWh), nicht Wattstunden (Wh).
Um von Wattstunden zu Kilowattstunden zu kommen,
müssen wir den Zahlwert durch 1000 teilen.

Wenn wir hier die Namen unserer Werte klug wählen,
lässt sich die Abfolge unserer Berechnungen ziemlich leicht nachvollziehen
und fast wie deutsche Sätze lesen:

```
fun berechneStromverbrauchProJahrKWH(stromverbrauchW: Double, stundenAmTag: Double): Double {
    val stromverbrauchProTagWH = stromverbrauchW * stundenAmTag
    val stromverbrauchProJahrWH = stromverbrauchProTagWH * 365
    val stromverbrauchProJahrKWH = stromverbrauchProJahrWH / 1000
    return stromverbrauchProJahrKWH
}
```

Wir müssten hier die einzelnen Zwischenschritte nicht einzeln aufführen.
Kotlin könnte auch diesen Code problemlos ausführen:

```
fun berechneStromverbrauchProJahrKWH(stromverbrauchW: Double, stundenAmTag: Double): Double {
    return stromverbrauchW * stundenAmTag * 365 / 1000
}
```

Die entscheidende Frage ist hier also wieder,
was für uns und andere Menschen leichter verständlich ist.
Und das ist natürlich oft Geschmackssache:
Manche werden die erste Variante übersichtlicher finden,
manche die zweite.
Wählt selbst, was euch besser gefällt!

## Übergabe von Werten an andere Funktionen

Unsere Funktion `berechneStromverbrauchProJahrKWH` ist nun einsatzbereit.
In unserer Hauptfunktion müssen wir aber noch eine Sache verändern.
Wir haben in der Signatur der Funktion `berechneStromverbrauchProJahrKWH`
angegeben, dass sie zwei Eingabewerte benötigt.
Diese müssen wir beim Aufruf der Funktion mit übergeben.
Unsere Hauptfunktion muss so aussehen:

```
fun main() {

    println("Hallo! Bitte gib den Stromverbrauch des Geräts ein [in Watt]:")
    val stromverbrauchW = liesZahl()

    println("Bitte gib an, wie viele Stunden pro Tag das Gerät benutzt wird:")
    val stundenAmTag = liesZahl()
    
    val verbrauchProJahrKWH = berechneVerbrauchProJahrKWH(stromverbrauchW, stundenAmTag)
    println("Der Energieverbrauch pro Jahr beträgt [in kWh]:")
    println(verbrauchProJahrKWH)
}
```

Denn: Unsere beiden benannten Werte `stromverbrauchW` und `stundenAmTag`
haben wir in der Hauptfunktion definiert.
Das bedeutet, dass sie grundsätzlich auch nur dort verfügbar sind!
Wenn wir aus unserer Hauptfunktion in eine andere Funktion springen wollen,
müssen wir alle Werte,
auf die die neue Funktion zugreifen soll,
ausdrücklich als Eingabewerte übergeben
und sie beim Funktionsaufruf in die Klammern schreiben.
Dabei ist die Reihenfolge wichtig:
Sie muss übereinstimmen mit der Reihenfolge,
die wir in der Signatur der Funktion aufgeschrieben haben.

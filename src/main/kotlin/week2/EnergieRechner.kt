package week2

fun liesZahl(): Double {
    val eingabeText = readln()
    val eingabeZahl = eingabeText.toDouble()
    return eingabeZahl
}

fun berechneStromverbrauchProJahrKWH(stromverbrauchW: Double, stundenAmTag: Double): Double {
    val stromverbrauchProTagWH = stromverbrauchW * stundenAmTag
    val stromverbrauchProJahrWH = stromverbrauchProTagWH * 365
    val stromverbrauchProJahrKWH = stromverbrauchProJahrWH / 1000
    return stromverbrauchProJahrKWH
}

fun main() {

    println("Hallo! Bitte gib den Stromverbrauch des Geräts ein [in Watt]:")
    val stromverbrauchW = liesZahl()
    println("Bitte gib an, wie viele Stunden pro Tag das Gerät benutzt wird:")
    val stundenAmTag = liesZahl()

    val stromverbrauchProJahrKWH = berechneStromverbrauchProJahrKWH(stromverbrauchW, stundenAmTag)
    println("Der Gesamtstromverbrauch pro Jahr beträgt [in kWh]:")
    println(stromverbrauchProJahrKWH)
}
